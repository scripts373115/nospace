#!/bin/bash

## Script de renommage d'un fichier passé en paramètre
## Debian 9.11 && Ubuntu 22.04.4
## Louis TEILLAY && Adrien LANDEMAINE
## 08/04/2024
## Version 2.0

verbose=false
file=null
paramDir=null
while getopts 'f:r:v' opt; do

        case "$opt" in
                f)
                        file=$2
                        shift
                        ;;
                r)
                        paramDir=$2
                        shift
                        ;;
                v)      
                        verbose=true
                        shift
                        ;;
        esac
done

# si on modifie qu'un seul fichier avec le paramêtre -f 
if [ "$file" != null ]; then
        #le fichier est corecte
        if [ -f "$file" -a $# -eq 1 ]; then
                nname=$(echo "$file" | sed 's/ /_/g')
                #log verbose
                if [ "$verbose" == true ]; then
                        echo "Modification du fichier: ${file} en: ${nname}"
                fi  
                mv "$1" "$nname"
        elif [ -d "$1" ]; then
                echo "$1 est un dossier. Merci de renseigner un fichier."
        else 
                echo "Merci de saisir un seul fichier correct"
        fi
# si on modifie tout les éléments depuis le dossier demandé de façon récursif 
elif [ "$paramDir" != null ]; then
        currdir="$paramDir"
        fromDir="$(pwd)"
        #gestion chemin relatif home dir
        if [[ "$currdir" == "~/"* ]]; then
                currdir="${HOME}${currdir:1}"
        fi

        #se positionne dans le chemin demandé s'il existe
        if [ -d "$currdir" ]; then
                cd "$currdir" || exit 1
        else
                echo "chemin non fonctionnel"
                exit 1
        fi
        #parcourir tout les dossier de manière recursif qui ne sont pas des dotfile ou dans des dotfiles
        find . -type d -not -path '*/.*' | while read -r dir; do
                nname=$(echo "$dir" | sed 's/ /_/g')
                if [ "$dir" != "$nname" ]; then
                        #log verbose
                        if [ "$verbose" == true ]; then
                                echo "Modification du dossier: ${dir} en: ${nname}"  
                        fi
                        mv "$dir" "$nname"
                fi
        done
        #parcourir tout les fichiers de manière recursif qui ne sont pas des dotfile ou dans des dotfiles
        find . -type f -not -path '*/.*' | while read -r file; do             
                nname=$(echo "$file" | sed 's/ /_/g')
                if [ "$file" != "$nname" ]; then
                        #log verbose
                        if [ "$verbose" == true ]; then
                                echo "Modification du fichier: ${file} en: ${nname}"
                        fi   
                        mv "$file" "$nname"
                fi
        done
        #retour dans le repertoire ou la commande à été lancé
        cd $fromDir > /dev/null || exit 1
else
        echo "Un problème est survenu lors de la récupération des paramètres"
fi