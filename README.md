# Nospace est un script permettant de renommer les fichiers dont le nom comporte des espaces en les remplaçant par des underscore.

Ce script en bash, nommé `nospace.sh`, permet de renommer les espaces dans les noms de fichiers et de répertoires en utilisant différentes options.

## Utilisation

```bash
chmod +x nospace.sh
./nospace.sh [-v] -f <nom_du_fichier> | -r <chemin_du_repertoire>
```

### Options

- `-f <nom_du_fichier>` : Renomme les espaces du fichier spécifié en remplaçant les espaces par des underscores `_`.

- `-r <chemin_du_repertoire>` : Renomme de manière récursive tous les noms de fichiers et de répertoires à partir du chemin spécifié en remplaçant les espaces par des underscores `_`.

- `-v` : Active le mode verbeux pour enregistrer les modifications effectuées.

### Exemple d'utilisation

```bash
./nospace.sh -f mon\ fichier\ avec\ espaces.txt
```
ou 

```bash
./nospace.sh -r /chemin/vers/le/repertoire
```

## Notes

- Assurez-vous d'avoir les permissions nécessaires pour modifier les fichiers et répertoires spécifiés.
- Utilisez l'option '-v' pour obtenir des informations sur les modifications effectuées.
- Utilisez avec précaution, il est recommandé de tester d'abord sur des données non critiques.

## Auteur

Ce script a été créé par [AdrienLdm](https://gitlab.com/Adrienldm) et [Wapax](https://gitlab.com/Wapax) et est distribué sous [licence MIT](LICENSE.md#licence-mit).
